<?php
// Pre-requisites:
// 	1. Url Re-writing (mod rewrite)
//	2. AJAX - We should have completed the Abstracting AJAX project
//	3. Regular expressions

header("Access-Control-Allow-Origin: *");
if($_SERVER['SERVER_NAME'] == "localhost"){
  //dev enviroment settings
  define("DEBUG_MODE", true);
  define("DB_HOST", "localhost");
  define("DB_USER", "root");
  define("DB_PASSWORD", "");
  define("DB_NAME", "card_web_service");
  define("SITE_ADMIN_EMAIL", "XXXXXXX");
  define("SITE_DOMAIN", $_SERVER['SERVER_NAME']);
  
} else{
  // PRODUCTION SETTINGS
  require("includes/database-info.php");
}

//error reporting
if(DEBUG_MODE){
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
}

///////////////////////////////////
// Set up the data access object
///////////////////////////////////
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
require("CardDataAccess.inc.php");
$da = new CardDataAccess($link);

///////////////////////////////////
// Handle the Request
///////////////////////////////////

// Gather all the information about the request
$url_path = isset($_GET['url_path']) ? $_GET['url_path'] : ""; // This is the path part of the URL being requested (see the .htaccess file)
$method = $_SERVER['REQUEST_METHOD'];
$request_body = file_get_contents('php://input');
$request_headers = getallheaders();

// This IF statement will check to see if the requested URL (and method) is supported by this web service
// If so, then we need to formulate the proper response, if not then we return a 404 status code
if($method == "GET" && empty($url_path)){

 	//Show the home page for this web service
  require("api-docs.php");
  die();

}else if($method == "GET" && ($url_path == "cards/" || $url_path == "cards")){

  try{
    get_all_cards();
  } catch (Exception $e){
    echo($e->getMessage());
  }
  
}else if($method == "POST" && ($url_path == "cards/" || $url_path == "cards")){
  	
  insert_card();
  
}else if($method == "GET" && preg_match('/^cards\/([0-9]*\/?)$/', $url_path, $matches)){
  // Get the id of the data from the regular expression
  $card_id = $matches[1];
  get_card_by_id($card_id);
}else if($method == "PUT" && preg_match('/^cards\/([0-9]*\/?)$/', $url_path, $matches)){

  // Get the id of the data from the regular expression
  //$data_id = $matches[1]; // we can get the data id from the request body
  update_card();  
  
} else if($method == "UPDATE_NON_ACTIVE" && preg_match('/^cards\/([0-9]*\/?)$/', $url_path, $matches)){
  update_card();
}else{

  header('HTTP/1.1 404 Not Found', true, 404);
  die("We're sorry, we can't find this page: {$_SERVER['REQUEST_URI']}");
}

/////////////////////////////////
// FUNCTIONS
/////////////////////////////////
function get_all_cards(){

  global $da, $url_path, $method, $request_body, $request_headers;
  
  // check to see if we need to sort the data
  $order_by = isset($_GET['order_by']) ? $_GET['order_by'] : null;
  
  // get the data from the database
  $cards = $da->get_all_cards($order_by);
  
  if(!$cards){
    header('HTTP/1.1 500 server error (fetching cards from the database)', true, 500);
    die();
  }
  
  // We'll default to returning data in JSON format unless the client
  $json = json_encode($cards);
  echo($json);
  die();
}


function insert_card(){

  global $da, $url_path, $method, $request_body, $request_headers;

  // The data for the data being inserted should be in the request body.
  // The data should be sent in the JSON format which means that the Content-Type header in the request SHOULD be set to application/json
  // But if it's not set properly, we'll just assume the data is coming in as JSON
  // In the future, we might also accept the data in the request body to be XML, which would mean that the Content-Type header is set to application/xml
  
  $new_card = null;
  
  if($request_headers['Content-Type'] == "application/xml"){
    die("TODO: convert XML card data into an associative array");    
  }else{
    // convert the json to an associative array
    // note: json_decode() will return false if it can't convert the request body (maybe because it's not valid json)
    $new_card = json_decode($request_body, true);
  }

  // TODO: validate the $new_data assoc array (make sure it has id and other keys)

  if($new_card){
    
    if($new_card = $da->insert_card($new_card)){
      // note that the data returned by insert_data() will have the id set (by the database auto increment 
      die(json_encode($new_card));
    }else{
      header('HTTP/1.1 500 server error (fetching cards from the database)', true, 500);
      die();  
    }
  
  }else{
    header('HTTP/1.1 400 - the data sent in the request is not valid', true, 400);
    die();
  }
}

function get_card_by_id($id){

  global $da, $url_path, $method, $request_body, $request_headers;

  $card = $da->get_card_by_id($id);
  var_dump($card);
  die();
  if($card){
    // header("Content-Type","application/json");
    die(json_encode($card));
  }else{
    header('HTTP/1.1 400 - the card id in the requested url is not in the database', true, 400);
    die();
  }
}


function update_card(){

  global $da, $url_path, $method, $request_body, $request_headers;

  if($card = json_decode($request_body, true)){

    if($method == "UPDATE_NON_ACTIVE"){
      $card = $da->set_non_active($card);
    } else {
      $card = $da->update_card($card);
    }
    
    if($card){
      die(json_encode($card));
    }else{
      header('HTTP/1.1 500 - Unable to update card in the database', true, 500);
      die();
    }

  }else{
    header('HTTP/1.1 400 - Invalid card data in the request body', true, 400);
    die();
  }

}
?>