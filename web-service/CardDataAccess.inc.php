<?php

class CardDataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all cards
	* @param $order_by 
	* @return 2d array
	*/
	function get_all_cards($order_by = null){
		// TODO: if the order_by param is not null we need to sort the cards
		$qStr = "SELECT	card_id, card_name, card_cmc, card_type, card_color, card_active FROM cards";

		if($order_by == "card_name" || $order_by == "card_cmc"){
			$qStr .= " ORDER BY " . $order_by;
		}
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_cards = array();

		while($row = mysqli_fetch_assoc($result)){

			$card = array();
			$card['card_id'] = htmlentities($row['card_id']);
			$card['card_name'] = htmlentities($row['card_name']);
			$card['card_cmc'] = htmlentities($row['card_cmc']);
			$card['card_type'] = htmlentities($row['card_type']);
			$card['card_color'] = htmlentities($row['card_color']);
			$card['card_active'] = htmlentities($row['card_active']);
			// die($card);
			$all_cards[] = $card;
		}

		return $all_cards;
	}


	/**
	* Get a card by its ID
	* @param $id
	* @return array
	*/
	function get_card_by_id($card_id){
		
		// TODO: if the order_by param is not null we need to sort the cards (should be by title or author)
		$qStr = "SELECT	card_id, card_name, card_cmc, card_type, card_color, card_active FROM cards WHERE card_id = " . mysqli_real_escape_string($this->link, $card_id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$card = array();
			$card['card_id'] = htmlentities($row['card_id']);
			$card['card_name'] = htmlentities($row['card_name']);
			$card['card_cmc'] = htmlentities($row['card_cmc']);
			$card['card_type'] = htmlentities($row['card_type']);
			$card['card_color'] = htmlentities($row['card_color']);
			$card['card_active'] = htmlentities($row['card_active']);
			return $card;

		}else{
			return null;
		}
			
	}


	function insert_card($card){
		
		// prevent SQL injection
		$card['card_name'] = mysqli_real_escape_string($this->link, $card['card_name']);
		$card['card_cmc'] = mysqli_real_escape_string($this->link, $card['card_cmc']);
		$card['card_type'] = mysqli_real_escape_string($this->link, $card['card_type']);
		$card['card_color'] = mysqli_real_escape_string($this->link, $card['card_color']);
		$card['card_active'] = mysqli_real_escape_string($this->link, $card['card_active']);
		

		$qStr = "INSERT INTO cards (
					card_name,
					card_cmc,
					card_type,
					card_color,
					card_active
				) VALUES (
					'{$card['card_name']}',
					'{$card['card_cmc']}',
					'{$card['card_type']}',
					'{$card['card_color']}',
					'{$card['card_active']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the card id that was assigned by the data base
			$card['card_id'] = mysqli_insert_id($this->link);
			// then return the card
			return $card;
		}else{
			$this->handle_error("unable to insert card");
		}

		return false;
	}

	function update_card($card){

		// prevent SQL injection
		$card['card_id'] = mysqli_real_escape_string($this->link, $card['card_id']);
		$card['card_name'] = mysqli_real_escape_string($this->link, $card['card_name']);
		$card['card_cmc'] = mysqli_real_escape_string($this->link, $card['card_cmc']);
		$card['card_type'] = mysqli_real_escape_string($this->link, $card['card_type']);
		$card['card_color'] = mysqli_real_escape_string($this->link, $card['card_color']);
		$card['card_active'] = mysqli_real_escape_string($this->link, $card['card_active']);

		$qStr = "UPDATE cards SET card_name ='{$card['card_name']}', card_cmc ='{$card['card_cmc']}', card_type ='{$card['card_type']}', card_color = '{$card['card_color']}', card_active = '{$card['card_active']}' WHERE card_id = " . $card['card_id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $card;
		}else{
			$this->handle_error("unable to update card");
		}

		return false;
	}
	
	function set_non_active($card){
		// prevent SQL injection
		$card['card_id'] = mysqli_real_escape_string($this->link, $card['card_id']);
		$card['card_name'] = mysqli_real_escape_string($this->link, $card['card_name']);
		$card['card_cmc'] = mysqli_real_escape_string($this->link, $card['card_cmc']);
		$card['card_type'] = mysqli_real_escape_string($this->link, $card['card_type']);
		$card['card_color'] = mysqli_real_escape_string($this->link, $card['card_color']);

		$qStr = "UPDATE cards SET card_name ='{$card['card_name']}', card_cmc ='{$card['card_cmc']}', card_type ='{$card['card_type']}', card_color = '{$card['card_color']}', card_active = 'false' WHERE card_id = " . $card['card_id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $card;
		}else{
			$this->handle_error("unable to set card to nonactive");
		}

		return false;
	}
}