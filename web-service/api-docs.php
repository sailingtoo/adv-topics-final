<!DOCTYPE html>
<html>
<head>
	<title>Card Service API Docs</title>
</head>
<body>

<h1>Official API Docs for the Cards Web Service</h1>
  <table border="1">
    <tr>
      <th>API CALL (ROUTE)</th>
      <th>METHOD</th>
      <th>ACTION</th>
    </tr>
    <tr>
      <td>cards/</td>
      <td>GET</td>
      <td>Get's all cards</td>
    </tr>
    <tr>
      <td>cards/</td>
      <td>POST</td>
      <td>
        Inserts a new card into the data base <br>
        Expects the Content-Type to be application/json
      </td>
    </tr>
    <tr>
      <td>cards/?order_by=x</td>
      <td>GET</td>
      <td>
        Get's all cards, ordered by a property of a card <br>
        Note: x could be 'card_name' or 'card_type'
      </td>
    </tr>
    <tr>
      <td>cards/x</td>
      <td>GET</td>
      <td>
        Get's a card by it's id property <br>
        ex: <b>cards/1</b> would fetch the card with an id of 1
      </td>
    </tr>
    <tr>
      <td>cards/x</td>
      <td>PUT</td>
      <td>
        Edits the card with id of x <br>
        Note: then Content-Type should be application/json
      </td>
    </tr>
    <tr>
      <td>card/x</td>
      <td>UPDATE_NON_ACTIVE</td>
      <td>
        Edits the card with id of x <br>
        Changes the active column to false
      </td>
    </tr>
  </table>
  ***By default, responses will return data in JSON format <br>
  ***BUT IF THE 'Accept' request header is set to 'application/xml' THEN WE RETURN THE DATA IN XML FORMAT

</body>
</html>