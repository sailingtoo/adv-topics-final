-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2017 at 11:55 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `card_web_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `card_id` int(11) NOT NULL,
  `card_name` varchar(64) NOT NULL,
  `card_cmc` int(11) NOT NULL,
  `card_type` varchar(32) NOT NULL,
  `card_color` varchar(32) NOT NULL,
  `card_active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`card_id`, `card_name`, `card_cmc`, `card_type`, `card_color`, `card_active`) VALUES
(1, 'Reality Smasher', 5, 'Creature', 'Colorless', 'true'),
(2, 'Forest', 0, 'Land', 'Colorless', 'true'),
(3, 'Force of Will', 78, 'Sorcery', 'Blue', 'true'),
(4, 'Mountain', 0, 'Land', 'Colorless', 'false'),
(5, 'Snapecaster Mage', 2, 'Land', 'Blue', 'true'),
(6, 'Four', 4, 'Land', 'Blue', 'true'),
(20, 'Card', 12, 'Sorcery', 'Black', 'true'),
(21, 'Jeff', 12, 'Artifact', 'Blue', 'true');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`card_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
