### What is this project about ###

This project connects two modules that have a user create, edit, delete, and list out trading cards. 
The application is asynchronous using ajax calls to the database. 
Link to the final product: http://www.sailingtoo.com/dist/

### Getting Set Up ###

This project uses NodeJS and Gulp.

### Modules ###

This project has two modules:

The card details (form) module is located here:
src/js/modules/card-details/card-details.js

The card list module is located here:
src/js/modules/card-details/card-list.js

The modules are connected by the js/main.js file.
Callbacks are sent between the modules.

### Building and Deploying the App ###

To compile the app for distribution:

* Open the terminal and cd to the project folder
* Enter this command to run the Gulp tasks: gulp
* The compiled code for distribution will be in the 'dist' folder
* Copy the dist folder to the production server
* Copy the web-service folder to the production server

### Copyright and Authors ###

* Jeffrey (Sailing Too) Young