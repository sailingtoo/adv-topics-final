var acme = acme || {};

acme.CardDataAccess = function() {

	var urlWebService;
	var ajax = acme.ajax;

	if(!ajax){
		throw new Error("The ajax does not exist.");
	}
	
	if(location.host == "localhost"){
		urlWebService = "http://localhost/adv-topics-final/web-service/cards/";
	} else {
		urlWebService = "http://www.sailingtoo.com/web-service/cards/";
	}

	function getAllCards(callback){

		acme.ajax.send({
			callback: callback,
			url: urlWebService,
			method: "GET"
		});
	}

	function getCardById(id, callback){

		acme.ajax.send({
			callback: callback,
			url: urlWebService +  id,
			method: "GET"
		});
	}

	function postCard(card, callback){

		acme.ajax.send({
			callback: callback,
			url: urlWebService,
			method: "POST",
			requestBody: JSON.stringify(card)
		});
	}
	

	function putCard(card, callback){

		acme.ajax.send({
			callback: callback,
			url: urlWebService,
			method: "PUT",
			requestBody: JSON.stringify(card)
		});
	}

	function nonActive(card, callback){

		acme.ajax.send({
			callback: callback,
			url: urlWebService,
			method: "UPDATE_NON_ACTIVE",
			requestBody: JSON.stringify(card)
		})
	}

	return{
		getAllCards: getAllCards,
		getCardById: getCardById,
		postCard: postCard,
		putCard: putCard,
		nonActive: nonActive
	}
}