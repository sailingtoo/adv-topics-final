var acme = acme || {};

acme.ajax = {

	send: function(options){

		var http = new XMLHttpRequest();
		var callback = options.callback;
		var url = options.url;
		var method = options.method;
		var headers = options.headers;
		var requestBody = options.requestBody;
		var errorCallback = options.errorCallback;

		http.open(method, url);

		if(headers){
			for (var key in headers){
				http.setRequestHeader(key, headers[key]);
			}
		}
		
		http.addEventListener("readystatechange", function(){
			if(http.readyState == 4 && http.status == 200){
				callback(http.responseText);
			} else if(http.readyState == 4){
				
				if (errorCallback) {
					errorCallback(http.status);
				} else {
					alert("Default error!");
				}
			}
		});
		
		http.send(requestBody);

	},
	error: function(errMsg){
		alert(errMsg);
	}
};