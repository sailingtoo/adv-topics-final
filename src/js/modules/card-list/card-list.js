/**
* @module acme
* @class CardList
*/
var acme = acme || {};

acme.CardList = {
	
	/**
	* @constructor
	* @param {DOM element} options.target 
	* @param {Object} options.cards
	* @param {function} options.selectCallback
	* @param {function} options.deleteCallback 
	*/
	init: function(options){

		// instance vars from options
		var target = options.target;
		var cards = options.cards;
		var selectCallback = options.selectCallback;
		var deleteCallback = options.deleteCallback;

		/*
		* If there is a target...
		* 1. Creates the UI
		* 2. Removes the existing EventListeners
		* 3. Adds back the EventHandlers to prevent duplication EventListeners conflicting
		*/
		if(target){
			target.appendChild(createUI());
			target.removeEventListener("click", listClickEvents);
			addEventHandlers();
		}
		
		/*
		* Creates the list of the card objects
		* @method createUI
		*/
		function createUI(){

			target.innerHTML = "";
			
			//instance vars
			var listDiv = document.createElement("div");
			listDiv.setAttribute("id", "listDiv");

			//loop through array of card objects
			for (var i = 0; i < cards.length; i++) {

				//variables
				var div = document.createElement("div");
				var divCard = document.createElement("div");
				var deleteButton = document.createElement("button");

				div.setAttribute("data-card-id", cards[i].card_id);
				div.setAttribute("class", "data-card-container");

				divCard.setAttribute("data-card-id", cards[i].card_id);
				divCard.setAttribute("class","data-card-info");

 				divCard.innerHTML += cards[i].card_name  
 					+ ": (Cost: " 
 					+ cards[i].card_cmc 
 					+ ") (Type: "
 					+ cards[i].card_type
 					+ ") (Color: "
 					+ cards[i].card_color
 					+") ";
 				
 				deleteButton.setAttribute("delete-card-id", cards[i].card_id);
 				deleteButton.innerHTML = "Delete";

 				//append tags
 				divCard.appendChild(deleteButton);
 				div.appendChild(divCard);
				listDiv.appendChild(div);
			}
			return listDiv;
		}

		/*
		* Adds the eventListeners to the target
		* @method addEventHandlers
		*/
		function addEventHandlers(){
			target.addEventListener('click', listClickEvents);
		}

		/*
		* Calls the selectCallback if the card was the target
		* Calls the deleteCallback if the btnDelete was the target
		* @method listClickEvents
		* @param {DOM element} evt.target
		*/
		function listClickEvents(evt){
			var eventTarget = evt.target;
			var card;
			var cardId = eventTarget.getAttribute("data-card-id");
			var deleteCardId = eventTarget.getAttribute("delete-card-id");

			if(cardId){
				card = getCardById(cardId);
				if(selectCallback){
					selectCallback(card);
				}
			} else if (deleteCardId) {
				card = getCardById(deleteCardId);
				if(deleteCallback){
					deleteCallback(card);
				}
			}
		}

		/*
		* Loops through array and find the card that has the id of the btnEdit or btnDelete
		* @method getCardById
		* @param {Number} cardId
		*/
		function getCardById(cardId){
			for (var i = 0; i < cards.length; i++) {
				if(Number(cards[i].card_id) == cardId){
					return cards[i];
				}
			}
			return null;
		}
		
		/**
		* Function takes the target and appends the creation of a new list.
		* @method refresh
		*/
		function refresh (){
			target.appendChild(createUI());
		}

		/**
		* Function sets the new cards object and calls the refresh function.
		* @method setCards
		* @param {Object} newCards The list of cards.
		*/
		function setCards(newCards){
			cards = newCards;
			refresh();
		}
		
		//Public API
		return {
			refresh: refresh,
			setCards: setCards
		};
	}
};