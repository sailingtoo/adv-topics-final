/**
* @main The acme module takes a list of cards and contains a form that edits the list through ajax calls. 
* @module acme
* @class CardDetails
*/
var acme = acme || {};

acme.CardDetails = {
	
	/**
	* @constructor
	* @param {DOM element} options.target  The target works best with a div.
	* @param {function} options.saveCallback  Callback when the save button is pressed.
	* @param {Object} options.card 
	*/
	init: function(options){

		//instance vars from the options object
		var target = options.target;
		var saveCallback = options.saveCallback;
		var card = options.card;

		//creating the inital UI form for book
		target.innerHTML = createUI();

		//createUI instance variables
		var txtName = target.querySelector('.txtName');
		var txtCmc = target.querySelector('.txtCmc');
		var selType = target.querySelector('.selType');
		var selColor = target.querySelector('.selColor');
		var txtActive = target.querySelector('.txtActive');

		//createUI buttons
		var btnSave = target.querySelector('.btnSave');
		var btnReset = target.querySelector('.btnReset');

		//button onclick functions
		btnSave.onclick = function(){
			if(validate() && saveCallback){
				getDataFromUI();
				saveCallback(card);
				clearInputForm();
				resetCard();
			}
		};
		btnReset.onclick = function(){
			clearInputForm();
			clearValidationMessages();
			resetCard();
		}

		/*
		* If card ... 
		* 1. Sets the card to the values of the inputs
		* 2. Updates the UI with the new card
		* Else
		* 1. Creates a default card object
		*/ 
		if(card){
			updateUI();
		} else {
			resetCard();
		}

		function resetCard(){
			card = {card_id: "", card_name: "", card_cmc: "", card_type: 0, card_color: 0, card_active: "true"};
		}

		// creating the module html
		function createUI(){
			var html =  '<b>Card Name:</b><br> \
						<input type="text" class="txtName" maxlength="32"/> \
						<span id="vName" class="validation"></span><br> \
						<b>Converted Mana Cost:</b><br> \
						<input type="text" class="txtCmc" maxlength="2"/> \
						<span id="vCmc" class="validation"></span><br> \
						<b>Card Type:</b><br> \
						<select class="selType"> \
							<option value="0">Choose One...</option> \
							<option value="Instant">Instant</option> \
							<option value="Sorcery">Sorcery</option> \
							<option value="Land">Land</option> \
							<option value="Artifact">Artifact</option> \
							<option value="Enchantment">Enchantment</option> \
							<option value="Creature">Creature</option> \
							<option value="Planeswalker">Planeswalker</option> \
							<option value="Multiple">Multiple</option> \
						</select> \
						<span id="vType" class="validation"></span><br> \
						<b>Card Color:</b><br> \
						<select class="selColor"> \
							<option value="0">Choose One...</option> \
							<option value="Red">Red</option> \
							<option value="White">White</option> \
							<option value="Blue">Blue</option> \
							<option value="Green">Green</option> \
							<option value="Black">Black</option> \
							<option value="Multiple">Multiple</option> \
							<option value="Colorless">Colorless</option> \
						</select> \
						<span id="vColor" class="validation"></span><br> \
						<input type="text" class="txtActive" style="display: none;" value="true"> \
						<span id="vActive" class="validation"></span><br> \
						<button class="btnSave">Save</button> \
						<button class="btnReset">Reset</button><br><br> \
						<div id="vError">To add a new card, click the Reset button to clear the form.</div>';
			return html;
		}

		//setting the values of the inputs to the book object
		function updateUI(){
			
			if(card){
				txtName.value = card.card_name;
				txtCmc.value = card.card_cmc;
				selType.value = card.card_type;
				selColor.value = card.card_color;
				txtActive.value = card.card_active;
			}
		}

		//setting the book values of the inputs to the book object
		function getDataFromUI(){
			//taking the value out of the textboxes
			card['card_name'] = txtName.value;
			card['card_cmc'] = txtCmc.value;
			card['card_type'] = selType.value;
			card['card_color'] = selColor.value;
			card['card_active'] = txtActive.value;
		}

		//validate the inputs
		function validate(){

			//instance varables for the validation messages
			var vName = target.querySelector('#vName');
			var vCmc = target.querySelector('#vCmc');
			var vType = target.querySelector('#vType');
			var vColor = target.querySelector('#vColor');
			var vActive = target.querySelector('#vActive');
			var vError = target.querySelector('#vError');
			
			var isValid = true;
			var focusOn = null;

			clearValidationMessages();

			//valaidating that the input tags have a value
			if(!txtName.value || txtCmc.value.length > 32){
				isValid = false;
				vName.innerHTML = "Enter a valid name.";
				vName.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = txtName;
				}
			}

			if(!txtCmc.value){
				isValid = false;
				vCmc.innerHTML = "Enter a converted mana cost.";
				vCmc.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = txtCmc;
				}
			} else if (isNaN(txtCmc.value) || txtCmc.value < 0 || txtCmc.value.length > 2){
				isValid = false;
				vCmc.innerHTML = "Enter a valid number.";
				vCmc.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = txtCmc;
				}
			}

			if(!selType.value){
				isValid = false;
				vType.innerHTML = "Select a type.";
				vType.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = selType;
				}
			} else if (selType.value == 0){
				isValid = false;
				vType.innerHTML = "Select a type.";
				vType.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = selType;
				}
			}

			if(!selColor.value){
				isValid = false;
				vColor.innerHTML = "Enter a color.";
				vColor.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = selColor;
				}
			} else if (selColor.value == 0){
				isValid = false;
				vColor.innerHTML = "Enter a color.";
				vColor.style.display = "inline";
				//focus
				if(focusOn == null){
					focusOn = selColor;
				}
			}

			if(txtActive.value != "true"){
				isValid = false;
				vActive.innerHTML = "Active should be set to true";
				vActive.style.display = "inline";
			}

			if(isValid == false){
				focusOn.focus();
			}

			return isValid;
		}

		/**
		* Sets the newCard param to the variable card. Then it updates the form.
		* @method setCard
		* @param {Object} card Card by id pulled from the list.
		*/
		function setCard(newCard){
			card = newCard;
			updateUI();
		}

		//clears the validation span tags
		function clearValidationMessages(){

			var vMessages = document.querySelectorAll(".validation");

			//clear out all validation messages
			for(var x = 0; x < vMessages.length; x++){
				vMessages[x].style.display = "none";
			}
		}

		/**
		* Resets the input values to defaults.
		* @method clearInputForm
		*/
		function clearInputForm(){
			txtName.value = "";
			txtCmc.value = "";
			selType.value = 0;
			selColor.value = 0;
			txtActive.value = "true";
		}

		//public API
		return {
			setCard: setCard,
			clearInputForm: clearInputForm
		}
	}
};