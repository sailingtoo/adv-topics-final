window.addEventListener("load", function(){

	// Set up all the modules here...
	//variables
	var cardDetails;
	var cardList;
	var card;
	var da = acme.CardDataAccess();

	acmeAjaxSend();

	//GET request sent to the server to return all cards
	function acmeAjaxSend (){
		var urlWebService;
		if(location.host == "localhost"){
			urlWebService = "http://localhost/adv-topics-final/web-service/cards/";
		} else {
			urlWebService = "http://www.sailingtoo.com/web-service/cards/";
		}
		
		acme.ajax.send({
			method: "GET",
			url: urlWebService,
			callback: setUpCardList
		});
		
	}
		
	// setting up the cardList
	function setUpCardList(response){
		var cards = JSON.parse(response);
		var div = document.getElementById("card-list");
		var activeCards = new Array();
		var iActive = 0;

		for (var i = 0; i < cards.length; i++) {
			
			if(cards[i].card_active == "true"){
				activeCards[iActive] = cards[i];
				iActive ++;
			}
		}

		if(cardList){
			cardList.setCards(activeCards);
		} else {
			cardList = acme.CardList.init({
				target: div, 
				cards: activeCards,
				// cards: cards,
				selectCallback: function(card){
					// alert(card);
					cardDetails.setCard(card);
				},
				deleteCallback: function(card){
					
					cardDetails.clearInputForm();
					da.nonActive(card, function(){
						acmeAjaxSend();
					});
				}
			});
		}
	}

	//setting up the cardDetails
	cardDetails = acme.CardDetails.init({
		target: document.getElementById("card-details"),
		saveCallback: function(card){

			if(card.card_id > 0){
				da.putCard(card, function(){
					//reloads the cardList with the updated card
					cardList.refresh();
				});
				
			} else {
				da.postCard(card, function(){
					//GET method that recreates the cardList
					acmeAjaxSend();
				});
			}
		}
	});
});