YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "CardDetails",
        "CardList"
    ],
    "modules": [
        "acme"
    ],
    "allModules": [
        {
            "displayName": "acme",
            "name": "acme"
        }
    ],
    "elements": []
} };
});